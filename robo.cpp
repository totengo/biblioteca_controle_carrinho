#include "Arduino.h"
#include "robo.h"
#include <Servo.h>

//off1: esquerda; off2: direita

void calcula_limites(float& limp, float& limn, float val1, float val2);

robo::robo(float off1, float off2, int pinesq, int pindir) {
    this->off1=off1;
    this->off2=off2;
    this->pin_dir=pindir; this->servoDir.attach(this->pin_dir);
    this->pin_esq=pinesq; this->servoEsq.attach(this->pin_esq);
    calcula_limites(this->limp, this->limn, this->off1, this->off2);

    /*Serial.begin(9600);
    Serial.println(this->limp);
    Serial.println(this->limn);*/
}

robo::robo(int pinesq, int pindir) {
    this->off1=90;
    this->off2=90;
    this->pin_dir=pindir; this->servoDir.attach(this->pin_dir);
    this->pin_esq=pinesq; this->servoEsq.attach(this->pin_esq);
    calcula_limites(this->limp, this->limn, 90, 90);

    /*Serial.begin(9600);
    Serial.println(this->limp);
    Serial.println(this->limn);*/
}

void calcula_limites(float& limp, float& limn, float val1, float val2) {
    limp=(val1>val2)?180-val1:180-val2;
    limn=(val1>val2)?val2:val1;
}

void robo::tras(float speed) {
    this->servoEsq.write(this->off1-this->limn*speed/100.0);
    delay(200);
    this->servoDir.write(this->off2+this->limp*speed/100.0);
    delay(200);
}

void robo::frente(float speed) {
    this->servoEsq.write(this->off1+this->limp*speed/100.0);
    delay(200);
    this->servoDir.write(this->off2-this->limn*speed/100.0);
    delay(200);
}

void robo::curva_parada(float speed, sentidos sentido) {
    if(sentido) {
        this->servoEsq.write(this->off1-this->limn*speed/100.0);
        delay(200);
        this->servoDir.write(this->off2-this->limn*speed/100.0);
        delay(200);
    } else {
        this->servoDir.write(this->off2+this->limp*speed/100.0);
        delay(200);
        this->servoEsq.write(this->off1+this->limp*speed/100.0);
        delay(200);
    }
}

void robo::dir_power(float speed, int sentido) {
    if(sentido) 
        this->servoDir.write(this->off2+this->limp*speed/100.0);
        delay(200);
    else 
        this->servoDir.write(this->off2-this->limn*speed/100.0);
        delay(200);
}

void robo::esq_power(float speed, int sentido) {
    if(sentido)
        this->servoEsq.write(this->off1+this->limp*speed/100.0);
        delay(200);
    else 
        this->servoEsq.write(this->off2-this->limn*speed/100.0);
        delay(200);
}