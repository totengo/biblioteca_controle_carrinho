#ifndef ROBO_BIB
#define ROBO_BIB

#include <Servo.h>

#define TRAS 1
#define FRENTE 0

typedef enum sentidos {
    DIR=0,
    ESQ=1
} sentidos;

class robo {
    public:
        robo(float off1, float off2, int pinesq, int pindir);
        robo(int pinesq, int pindir);
        void frente(float speed);
        void tras(float speed);
        void curva_parada(float speed, sentidos sentido);
        void dir_power(float speed, int sentido);
        void esq_power(float speed, int sentido);
    private:
        int pin_dir, pin_esq;
        Servo servoEsq, servoDir;
        float off1, off2, limp, limn;
};

#endif